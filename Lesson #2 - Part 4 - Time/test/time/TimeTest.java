package time; 

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author Harman Bath
 *
 */

public class TimeTest {

	@Test
		public void testGetTotalMillisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliSeconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected=NumberFormatException.class) //WHEN USING expected use a fail as seen below 
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliSeconds("12:05:05:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getTotalMilliSeconds("12:05:05:999");
		assertTrue("Invalid", totalMilliseconds == 999); 
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getTotalMilliSeconds("12:05:05:1000");
		fail("Invalid number of milliseconds");
	}
	
	@Test
		public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match results", totalSeconds == 3661); 
	}
	
	@Test (expected=NumberFormatException.class)
		public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A"); 
		fail("The time provided is not valid"); 
		}
	
	@Test
		public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:59"); 
		assertTrue("The time provided does not match results", totalSeconds == 59); 
	}
	
	@Test (expected=NumberFormatException.class)
		public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid"); 
	}
}
